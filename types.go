package query

import (
	sdk "github.com/pokt-network/pocket-core/types"
	types2 "github.com/pokt-network/pocket-core/x/auth/types"
	nodeTypes "github.com/pokt-network/pocket-core/x/nodes/types"
	abci "github.com/tendermint/tendermint/abci/types"
	"github.com/tendermint/tendermint/libs/common"
	"github.com/tendermint/tendermint/types"
	"math/big"
)

type BalanceResponse struct {
	Balance *big.Int `json:"balance"`
}

type HeightResponse struct {
	Height int64 `json:"height"`
}

type RPCResultTx struct {
	Hash     common.HexBytes        `json:"hash"`
	Height   int64                  `json:"height"`
	Index    uint32                 `json:"index"`
	TxResult abci.ResponseDeliverTx `json:"tx_result"`
	Tx       types.Tx               `json:"tx"`
	Proof    types.TxProof          `json:"proof,omitempty"`
}

type StdTx struct {
	Msg       sdk.Msg             `json:"msg" yaml:"msg"`
	Fee       sdk.Coins           `json:"fee" yaml:"fee"`
	Signature types2.StdSignature `json:"signature" yaml:"signature"`
	Memo      string              `json:"memo" yaml:"memo"`
	Entropy   int64               `json:"entropy" yaml:"entropy"`
}

type ResultTx struct {
	Hash     common.HexBytes        `json:"hash"`
	Height   int64                  `json:"height"`
	Index    uint32                 `json:"index"`
	TxResult abci.ResponseDeliverTx `json:"tx_result"`
	Tx       types.Tx               `json:"tx"`
	Proof    types.TxProof          `json:"proof,omitempty"`
	StdTx    types2.StdTx           `json:"stdTx"`
}

type RPCResultTxSearch struct {
	Txs        []*RPCResultTx `json:"txs"`
	TotalCount int            `json:"total_count"`
}

type RPCResultNodes struct {
	Result     []*nodeTypes.Validator `json:"result"`
	TotalPages int                    `json:"total_pages"`
	Page       int                    `json:"page"`
}