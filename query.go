package query

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/pokt-network/pocket-core/app"
	"github.com/pokt-network/pocket-core/app/cmd/cli"
	"github.com/pokt-network/pocket-core/app/cmd/rpc"
	"github.com/pokt-network/pocket-core/types"
	nodeTypes "github.com/pokt-network/pocket-core/x/nodes/types"
	"io/ioutil"
	"net/http"
	"strconv"
)

func RPC(path string, jsonArgs []byte, result interface{}) error {
	//cliURL := app.GlobalConfig.PocketConfig.RemoteCLIURL + ":" + app.GlobalConfig.PocketConfig.RPCPort + path
	cliURL := app.GlobalConfig.PocketConfig.RemoteCLIURL + path
	req, err := http.NewRequest("POST", cliURL, bytes.NewBuffer(jsonArgs))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	bz, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	res, err := strconv.Unquote(string(bz))
	if err == nil {
		bz = []byte(res)
	}
	if resp.StatusCode == http.StatusOK {
		err = json.Unmarshal(bz, result)
		return err
	}
	return fmt.Errorf("the http status code was not okay: %d, and the status was: %s, with a response of %v", resp.StatusCode, resp.Status, string(bz))
}

func Tx(hash string) RPCResultTx {
	type txReq struct {
		Hash	string `json:"hash"`
	}
	var res RPCResultTx
	req, err := json.Marshal(txReq{hash})
	if err != nil {
		return RPCResultTx{}
	}
	err = RPC(cli.GetTxPath, req, &res)
	if err != nil {
		return RPCResultTx{}
	}
	return res
}

func Height() int64 {
	var res = HeightResponse{}
	err := RPC(cli.GetHeightPath, []byte{}, &res)
	if err != nil {
		return -1
	}
	return res.Height
}

func BlockTx(height int64) []ResultTx {
	params := rpc.PaginatedHeightParams{
		Height:  height,
		Page:    0,
		PerPage: 100,
		Prove:   false,
	}
	j, err := json.Marshal(params)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	var res = &RPCResultTxSearch{}
	err = RPC(cli.GetBlockTxsPath, j, res)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	txs := res.Txs
	var result = make([]ResultTx, 0, res.TotalCount)
	for _, tx := range txs {
		if tx == nil {
			continue
		}
		result = append(result, ResultTx{
			Hash:     tx.Hash,
			Height:   tx.Height,
			Index:    tx.Index,
			TxResult: tx.TxResult,
			Tx:       tx.Tx,
			Proof:    tx.Proof,
			StdTx:    app.UnmarshalTx(tx.Tx),
		})
	}
	return result
}

var nodes = make(map[int64][]*nodeTypes.Validator)

func Nodes(height int64) []*nodeTypes.Validator {
	if v, ok := nodes[height]; ok {
		return v
	}
	var err error
	opts := nodeTypes.QueryValidatorsParams{
		Blockchain: "",
		Page:       0,
		Limit:      0,
	}
	opts.StakingStatus = types.Staked
	opts.JailedStatus = 2
	params := rpc.HeightAndValidatorOptsParams{
		Height: int64(height),
		Opts:   opts,
	}
	j, err := json.Marshal(params)
	if err != nil {
		fmt.Println("marshal", err)
		return nil
	}
	//fmt.Println(string(j))
	var res = &RPCResultNodes{}
	err = RPC(cli.GetNodesPath, j, res)
	if err != nil {
		fmt.Println("rpc", err)
		return nil
	}
	nodes[height] = res.Result
	return res.Result
}

func Node(address string, height int64) *nodeTypes.Validator {
	params := rpc.HeightAndAddrParams{
		Height:  int64(height),
		Address: address,
	}
	j, err := json.Marshal(params)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	var res = &nodeTypes.Validator{}
	err = RPC(cli.GetNodePath, j, res)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	return res
}

func Balance(address string, height int64) *BalanceResponse {
	params := rpc.HeightAndAddrParams{
		Height:  height,
		Address: address,
	}
	j, err := json.Marshal(params)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	var res = &BalanceResponse{}
	err = RPC(cli.GetBalancePath, j, res)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	return res
}
