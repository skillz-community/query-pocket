module gitlab.com/skillz-community/query-pocket

go 1.13

replace github.com/tendermint/tendermint => github.com/pokt-network/tendermint v0.32.11-0.20200803010950-2b00c2006f24

require (
	github.com/pokt-network/pocket-core v0.0.0-20200810160221-eb3da99a409a
	github.com/tendermint/tendermint v0.33.7
)
